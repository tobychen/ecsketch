#include <AFMotor.h>
AF_DCMotor MLeft(2);
AF_DCMotor MRight(3);

#include <Wire.h>

const int LightLeft = 51, LightRight = 49;

#include <PS2Mouse.h>
#define MOUSE_DATA 14
#define MOUSE_CLOCK 15
PS2Mouse PMouse(MOUSE_CLOCK, MOUSE_DATA, STREAM);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Wire.begin();
  PMouse.initialize();
  //delay(2000);
  //changeAddress(0x73, 0xC5);
  //while(1);
}

int CorrectDir = 0;
int CompassReading = 0;
int FullSpeed = 255;
long TBegin = 0;
long TDuration = 4000;
float Disp;

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("Ready!");
  pinMode(53, INPUT_PULLUP);
  int k = digitalRead(53);
  if (k == HIGH)
    return;
  delay(1000);

  for (int i = 0; i < 10; i++) {
    readCompass(0x73);
    delay(100);
  }
  CorrectDir = CompassReading;
  Disp = 0.0f;
  TBegin = millis();

  bool bEnded = false;
  while (!bEnded) {
    delay(20);
    readCompass(0x73);
    int degOff = CompassReading - CorrectDir;
    if (degOff < 0) {
      degOff = 3600 + degOff;
    }
    if (degOff >= 3600) {
      degOff = degOff - 3600;
    }
    if (degOff >= 1800) {
      degOff = -(3600 - degOff);
    }
    Serial.println(degOff);
    if (abs(degOff) > 25) {
      if (degOff < 0) { // Left
        lightSwitch(true, false);
        MLeft.setSpeed(FullSpeed);
        MRight.setSpeed(FullSpeed * 0.9);
      }
      else { // Greater than 0, right
        lightSwitch(false, true);
        MLeft.setSpeed(FullSpeed * 0.9);
        MRight.setSpeed(FullSpeed);
      }
    }
    else {
      lightSwitch(false, false);
      MLeft.setSpeed(FullSpeed);
      MRight.setSpeed(FullSpeed);
    }
    MLeft.run(FORWARD);
    MRight.run(FORWARD);

    int data[3];
    PMouse.report(data);
    Disp += sqrt((float)sq(data[1]) + (float)sq(data[2]));
    if (/*millis() > TBegin + TDuration &&*/ Disp > 30000) {
      bEnded = true;
      lightSwitch(false, false);
    }
    else if (millis() > TBegin + 9000) {
      bEnded = true;
      lightSwitch(true, true);
    }
  }
  MLeft.run(RELEASE);
  MRight.run(RELEASE);
}

void readCompass(int address) {
  Wire.beginTransmission(address);
  Wire.write(byte(0x00));
  Wire.write(byte(0x31));
  Wire.endTransmission();
  delay(10);
  Wire.beginTransmission(address);
  Wire.write(byte(0x01));
  Wire.endTransmission();
  Wire.requestFrom(address, 1);
  if (1 <= Wire.available()) {
    CompassReading = Wire.read();
    CompassReading = CompassReading << 8;
  }
  Wire.beginTransmission(address);
  Wire.write(byte(0x02));
  Wire.endTransmission();
  Wire.requestFrom(address, 1);
  if (1 <= Wire.available()) {
    CompassReading |= Wire.read();
    //Serial.println(CompassReading);
  }
}

void changeAddress(byte oldAddress, byte newAddress) {
  Wire.beginTransmission(oldAddress);
  Wire.write(byte(0x00));
  Wire.write(byte(0xA0));
  Wire.endTransmission();

  Wire.beginTransmission(oldAddress);
  Wire.write(byte(0x00));
  Wire.write(byte(0xAA));
  Wire.endTransmission();

  Wire.beginTransmission(oldAddress);
  Wire.write(byte(0x00));
  Wire.write(byte(0xA5));
  Wire.endTransmission();

  Wire.beginTransmission(oldAddress);
  Wire.write(byte(0x00));
  Wire.write(newAddress);
  Wire.endTransmission();
}

void lightSwitch(bool l, bool r) {
  if (l)
    digitalWrite(LightLeft, HIGH);
  else
    digitalWrite(LightLeft, LOW);
  if (r)
    digitalWrite(LightRight, HIGH);
  else
    digitalWrite(LightRight, LOW);
}

