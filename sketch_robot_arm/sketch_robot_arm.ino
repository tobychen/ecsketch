#include <Adafruit_MotorShield.h>
#include <Servo.h>

// Joystick Pins:
const int js_ch1 = A0; // Up and down
const int js_ch2 = A3; // Left and right
const int js_switch = 2;
// Motor Pins:
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *ClawDC = AFMS.getMotor(3);
Adafruit_DCMotor *PositionMotor = AFMS.getMotor(4);

int value_ch1, value_ch2, control_mode = 0;
Servo ServoClaw, Servo2;
int ServoClawPos = 0, servo_2_pos = 0;

void setup() {
  Serial.begin(9600);
  AFMS.begin();
  pinMode(js_switch, INPUT_PULLUP);
  ServoClaw.attach(9);
  Servo2.attach(10);

  //ClawDC->setSpeed(255);
  //ClawDC->run(FORWARD);
  //while(1);
}

void loop() {
  read_input();
  if(control_mode % 2 == 0)
    manipulate_position();
  else
    manipulate_claw();
  ServoClaw.write(ServoClawPos);
  Servo2.write(servo_2_pos);
}

void read_input() {
  value_ch1 = analogRead(js_ch1) - 487;
  value_ch2 = analogRead(js_ch2) - 508;
  bool change_mode = button_debounce(js_switch);
  if(change_mode) {
    Serial.println("Cool");
    control_mode += 1;
    control_mode %= 2;
    Serial.println(control_mode);
  }
}

void manipulate_position() {
  /* if(abs(value_ch1) < 50) {
    PositionMotor->run(RELEASE);
  }
  else {
    Serial.println(constrain(map(abs(value_ch1), 0, 512, 0, 600), 0, 150));
    PositionMotor->setSpeed(constrain(map(abs(value_ch1), 0, 512, 0, 600), 0, 150));
    PositionMotor->run(value_ch1 > 0 ? FORWARD : BACKWARD);
  }*/

  if(abs(value_ch2) < 30) {
    //PositionMotor->run(RELEASE);
  }
  else {
    Serial.println(servo_2_pos);
    servo_2_pos -= value_ch2 < 0 ? 1 : -1;
    delay(10);
    servo_2_pos = constrain(servo_2_pos, 20, 180);
  }
}

void manipulate_claw() {
  if(abs(value_ch1) > 50) {
    ClawDC->setSpeed(255);
    ClawDC->run(value_ch1 > 0 ? FORWARD : BACKWARD);
  }
  else {
    ClawDC->run(RELEASE);
  }

  if(abs(value_ch2) > 50) {
    ServoClawPos -= value_ch2 / 255;
    delay(10);
    ServoClawPos = constrain(ServoClawPos, 0, 180);
  }
}

#define BTN_PRESSED LOW
bool button_debounce(const int pin) {
  static int button_last_state = HIGH;
  static long mute_button_until = 0;
  int button_state = digitalRead(pin);
  if(button_state != button_last_state) {
    button_last_state = button_state;
    long currTime = millis();
    if(currTime < mute_button_until)
      return false;
    mute_button_until = currTime + 200;
    if(button_state == BTN_PRESSED)
      return true;
  }
  return false;
}

