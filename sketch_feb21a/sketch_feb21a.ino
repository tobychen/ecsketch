#include <AFMotor.h>

const int potInPin = A8;
const int btnInPin[] = {53};
#define BTN_PRESSED LOW

const int encInPin[] = {34};

#define SON LOW
#define SOF HIGH
const int Digit[] = {22, 25, 27, 33};
const int Seg[] = {31, 23, 26, 30, 32, 29, 24, 28};
const int NumberConfig[][8] =
{ {SON, SON, SON, SON, SON, SON, SOF, SOF},
  {SOF, SON, SON, SOF, SOF, SOF, SOF, SOF},
  {SON, SON, SOF, SON, SON, SOF, SON, SOF},
  {SON, SON, SON, SON, SOF, SOF, SON, SOF},
  {SOF, SON, SON, SOF, SOF, SON, SON, SOF},
  {SON, SOF, SON, SON, SOF, SON, SON, SOF},
  {SON, SOF, SON, SON, SON, SON, SON, SOF},
  {SON, SON, SON, SOF, SOF, SOF, SOF, SOF},
  {SON, SON, SON, SON, SON, SON, SON, SOF},
  {SON, SON, SON, SON, SOF, SON, SON, SOF},

  {SOF, SOF, SOF, SOF, SOF, SOF, SOF, SOF},
  {SOF, SOF, SOF, SOF, SOF, SOF, SON, SOF}
};

AF_DCMotor motor1(1, MOTOR12_64KHZ);

int btnLastState[1];
long muteButtonUntil;

int encLastState[1];
int encCount[1];

void setup() {
  Serial.begin(9600);
  for(int i = 0; i < 4; i++)
    pinMode(Digit[i], OUTPUT);
  for(int i = 0; i < 8; i++)
    pinMode(Seg[i], OUTPUT);

  pinMode(btnInPin[0], INPUT_PULLUP);
  btnLastState[0] = HIGH;

  pinMode(encInPin[0], INPUT);
}

void countEncoder(int id) {
  int encState = digitalRead(encInPin[id]);
  if(encState != encLastState[id])
    encCount[id]++;
  encLastState[id] = encState;
}

// 0: No change
// 1: Push
// 2: Release
int pollButton(int id) {
  int btnState = digitalRead(btnInPin[id]);
  if(btnState != btnLastState[id]) {
    btnLastState[id] = btnState;

    if(millis() < muteButtonUntil)
      return 0;
    muteButtonUntil = millis() + 50;
    if(btnState == BTN_PRESSED)
      return 1;
    else
      return 2;
  }
  return 0;
}

bool displayByRot = false;

void loop() {
  countEncoder(0);
  displayNumber(displayByRot ? encCount[0] / 200 : encCount[0]);
  
  int sensorValue = analogRead(potInPin);
  Serial.print("sensor = ");
  Serial.println(sensorValue);
  sensorValue /= 20;

  displayNumber(sensorValue);
  if(pollButton(0) == 2) {
    ScheduleTask(&Drive, 1000, sensorValue * 1000);
    ScheduleTask(&Stop, 1000 + sensorValue * 1000, 0);
  }
  ExecuteTasks();
}

void Drive() {
  motor1.setSpeed(255);
  motor1.run(FORWARD);
}

void Stop() {
  motor1.run(RELEASE);
}

typedef void (*FUNC)();

struct TaskItem {
  FUNC Pointer;
  unsigned long StartTime;
  unsigned long EndTime;
  int State;
  /*
   * State meaning:
   * 0 Not used or ended
   * 1 Not yet started
   * 2 In progress
   */
} TaskArray[32];

void ScheduleTask(void (*task)(), unsigned long wait, unsigned long duration) {
  for(int i = 0; i < 32; i++) {
    if(TaskArray[i].State != 0)
      continue;
    TaskArray[i].Pointer = task;
    unsigned long now = millis();
    TaskArray[i].StartTime = now + wait;
    TaskArray[i].EndTime = now + wait + duration;
    TaskArray[i].State = 1;
    Serial.println("Got one");
    break;
  }
}

void ExecuteTasks() {
  unsigned long now = millis();
  for(int i = 0; i < 32; i++) {
    if(TaskArray[i].State == 1) {
      if(TaskArray[i].StartTime < now) {
        TaskArray[i].Pointer();
        TaskArray[i].State = 2;
      }
    }
    else if(TaskArray[i].State == 2) {
      if(TaskArray[i].EndTime > now) {
        TaskArray[i].Pointer();
      }
      else {
        TaskArray[i].State = 0;
      }
    }
  }
}

long LastDisplayNumber;

#define DISPLAY_BRIGHTNESS 500
#define DISPLAY_INTERVAL 10
void displayNumber(int n) {
  long beginTime = millis();
  if(beginTime - LastDisplayNumber < DISPLAY_INTERVAL)
    return;
  LastDisplayNumber = beginTime;
  for(int digit = 0; digit < 4; digit++) {
    if(n == 0 && digit != 0)
      continue;
    digitalWrite(Digit[digit], HIGH);
    lightNumber(n % 10);
    n /= 10;
    delayMicroseconds(DISPLAY_BRIGHTNESS);
    lightNumber(10);
    for(int allDigit = 0; allDigit < 4; allDigit++) {
      digitalWrite(Digit[allDigit], LOW);
    }
  }

  //while( (millis() - beginTime) < 10 );
}

void lightNumber(int n) {
  for(int i = 0; i < 8; i++) {
    digitalWrite(Seg[i], NumberConfig[n][i]);
  }
}

